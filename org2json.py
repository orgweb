# org2json
#  parse a org-mode file and convert it to JSON

__author__ = "Sridhar Ratnakumar <http://nearfar.org/>"

import re
from itertools import count

from simplejson import dumps

def rindexed(seq):
    """
    >>> l = [5,7,9]
    >>> print list(rindexed(l))
    [(2, 9), (1, 7), (0, 5)]
    """
    return zip(
        range(len(seq))[::-1],
        reversed(seq))

def reverse(iter):
    l = list(iter)
    l.reverse()
    return l


def org2py(orgtext):
    """Parse the given org file text and return the Python data structure

    >>> j = org2py(open('sample.org').read())
    >>> j=list(j)
    >>> j[-1]['text']
    'Projects'
    >>> j[-2]['text']
    'Whims'
    >>> j[-3]['text']
    'Online Stuff'
    >>> j[-4]['text']
    'Stuff'
    >>> j[-5]['text']
    'Travels'
    """
    lines = orgtext.splitlines()
    def e(i1, i2):
        return '\n'.join(lines[i1:i2])

    def by_star():
        last_index = len(lines)
        for index, line in rindexed(lines):
            if line.startswith("*"):
                yield [index, e(index, last_index)]
                last_index = index

    def hier(items):
        STARS_PAT = re.compile(r"^(\**) (.*)", re.DOTALL)
        TAGS_PAT  = re.compile(r"^(.*)\s((\w)*(:(\w)*)*:)$")
        def splititem(s):
            """
            >>> splititem("*** Foo Bar :TAG1:TAG2:")
            (3, 'Foo Bar', ('TAG1, 'TAG2'))
            >>> splititem("** write org-mode tutorial")
            (2, 'write org-mode tutorial')
            """
            match = STARS_PAT.match(s)
            stars, text = match.group(1), match.group(2)
            match = TAGS_PAT.match(s.splitlines()[0])
            tags = match and match.group(2).strip(':').split(':') or ()
            text = match and match.group(1) or text
            return len(stars), text, tags

        def node(text, children, tags):
            return {'text': text, 'children': children, 'tags': tags}

        istack = [[], [], [], [], [], [], [], [], []] # and so on ...

        pn = None
        for index, text in items:
            n, text, tags = splititem(text)
            assert n>0
            
            if n < pn:
                # up to the parent
                istack[n].append(node(text, reverse(istack[pn]), tags))
                istack[pn] = []
            else:
                # previous sibling OR child of one of the top nodes
                istack[n].append(node(text, [], tags))
            pn = n

        return reverse(istack[1])

    return hier(by_star())

def org2json(orgtext):
    return dumps(org2py(orgtext))


if __name__ == '__main__':
    from doctest import testmod
    testmod()
    print '--demo--'
    from pprint import pprint
    pprint( org2py(open('sample.org').read()) )
