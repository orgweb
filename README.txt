org-web

org-web is a simple web frontend for
[org-mode](http://orgmode.org/), an excellent planning tool for
emacs. org-web is still under development.

Contents:

org2json.py: parse org-mode files and convert to [JSON](http://json.org/)
